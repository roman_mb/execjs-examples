#!/usr/bin/env python
# encoding: utf-8
import sys
import execjs


### JS from file ###
from Naked.toolshed.shell import execute_js, muterun_js

# file.js
'''
var x = 10;
x = 10 - 5;
console.log(x);
function greet() {
      console.log("Hello World!");
}
greet()
'''

response = muterun_js('file.js')
if response.exitcode == 0:
    print(response.stdout)
else:
    sys.stderr.write(response.stderr)

### execute JS from text ###
response = execute_js('''var x = 10;
x = 10 - 5;
console.log(x);
function greet() {
      console.log("Hello World!");
};
''')

### call precompiled JS function ###
ctx = execjs.compile("""
    function add(x, y) {
        return x + y;
    }
""")
print(ctx)
print(ctx.call("add", 1, 2))

### eval JS
print(execjs.eval("'red yellow blue'.split(' ')"))


# My example - doesn't work
js_code_str = """function prod(a,b) {
  return a*b;
}

function sum(a,b) {
  return a+b;
}"""

print(execjs.eval(js_code_str))

